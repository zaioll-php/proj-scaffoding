FROM quay.io/keycloak/keycloak:latest

ENV KEYCLOAK_ADMIN=template
ENV KEYCLOAK_ADMIN_PASSWORD=change_me
ENV KC_FEATURES=token-exchange

ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start-dev"]