.PHONY :

RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
NC=\033[0m

no_cache := no
env := dev
app := app

proj := 
docker_env := ${env}.
ifeq "$(strip $(proj))" ""
    proj=$(dir)
endif
env_file=.env.${env}
ifeq "$(strip $(env))" "prod"
    env_file=.env
endif

tmp_dockerfile := docker/.Dockerfile
dir := $(shell basename $(CURDIR))
password := $(shell date | sha256sum | base64 | head -c32)

project: docker
	@printf "\n${NC}${RED}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   
	@printf "${env}\n"

	@cp docker/Dockerfile ${tmp_dockerfile}

	@sed -E -i -e "s/template/${proj}/;s/password/${password}/" docker/rabbitmq/${docker_env}Dockerfile
	@sed -E -i -e "s/template/${proj}/;s/change_me/${password}/" docker/keycloak/${docker_env}Dockerfile

	docker build -t ${proj}/webstack:${env} -f ${tmp_dockerfile} .
	cd docker/keycloak && docker build -t ${proj}/keycloak:${env} -f ${docker_env}Dockerfile . --no-cache && cd ../../
	cd docker/postgres && docker build -t ${proj}/postgres:${env} -f Dockerfile . && cd ../../
	cd docker/rabbitmq && docker build -t ${proj}/rabbitmq:${env} -f ${docker_env}Dockerfile . --no-cache && cd ../../

	git clone --single-branch --branch master git@gitlab.com:zaioll-php/proj-template.git
	@sed -E -i -e "s/KEYCLOAK_ADMIN\=template/KEYCLOAK_ADMIN=${proj}/;s/KEYCLOAK_ADMIN_PASSWORD\=password/KEYCLOAK_ADMIN_PASSWORD=${password}/;" proj-template/${env_file}
	@rm docker/.Dockerfile && cp -r ./docker proj-template/ && mv proj-template ../${proj} && cd ../${proj} && rm -fr .git && git init && git add . && git commit -m "Initial commit" && make init
	@git co -- .
	@printf "\n${NC}${GREEN}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   